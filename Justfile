alias b := build
alias s := site

build-static:
    npm run build

build: build-static
    stack build

site CMD: build
    stack exec site {{CMD}}
