module TheEffectFreeBlog (siteRoutes) where

import Hakyll
import TheEffectFreeBlog.Academic
import TheEffectFreeBlog.Blog
import TheEffectFreeBlog.Projects
import TheEffectFreeBlog.Static
import TheEffectFreeBlog.Utils (stripContent)

siteRoutes :: Rules ()
siteRoutes = do
    staticRoutes

    blogRoutes

    academicsRoutes

    projectsRoutes

    match "content/index.html" $ do
        route $ stripContent idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "content/blog/*"
            let indexCtx =
                    listField "posts" blogPostCtx (return posts)
                        `mappend` defaultContext

            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls

    match "templates/*" $ compile templateBodyCompiler
