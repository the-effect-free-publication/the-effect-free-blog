module TheEffectFreeBlog.Projects (projectsRoutes) where

import Hakyll

projectsRoutes :: Rules ()
projectsRoutes = do
    match "content/projects/*" $ do
        route $ setExtension "html"
        compile $
            pandocCompiler
                >>= relativizeUrls

    create ["projects.html"] $ do
        route idRoute
        compile $ do
            projects <- recentFirst =<< loadAll "content/projects/*"
            let projectsPageContext =
                    listField "items" defaultContext (return projects)
                        `mappend` constField "page-title" "Projects"
                        `mappend` defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/cards-page.html" projectsPageContext
                >>= loadAndApplyTemplate "templates/default.html" projectsPageContext
                >>= relativizeUrls
