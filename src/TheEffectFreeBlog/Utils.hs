module TheEffectFreeBlog.Utils (stripRoute, stripContent, compileAndLoadPost) where

import Hakyll

stripRoute :: String -> Routes
stripRoute prefix = customRoute $ drop (length prefix) . toFilePath

stripContent :: Routes -> Routes
stripContent = composeRoutes $ stripRoute "content/"

compileAndLoadPost :: Context String -> Compiler (Item String)
compileAndLoadPost ctx =
    pandocCompiler
        >>= loadAndApplyTemplate "templates/post.html" ctx
        >>= loadAndApplyTemplate "templates/default.html" ctx
        >>= relativizeUrls
