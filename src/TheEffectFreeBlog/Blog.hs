module TheEffectFreeBlog.Blog (blogPostCtx, blogRoutes) where

import qualified Data.Text as T
import Hakyll
import Text.Pandoc.Options
import TheEffectFreeBlog.Utils (stripContent)

replaceTeaserSeparatorWith :: String -> String -> String -> String
replaceTeaserSeparatorWith tsFrom tsTo html =
    T.unpack $ T.replace tsFrom' tsTo' html'
  where
    tsFrom' = T.pack tsFrom
    tsTo' = T.pack tsTo
    html' = T.pack html

replaceTeaserSeparator :: String -> String -> Item String -> Compiler (Item String)
replaceTeaserSeparator tsFrom tsTo item = return $ replaceTeaserSeparatorWith tsFrom tsTo <$> item

teaserSquareSeparator :: Item String -> Compiler (Item String)
teaserSquareSeparator = replaceTeaserSeparator "[more]" "<!--more-->"

teaserFieldSquareSeperator = teaserFieldWithSeparator "[more]"

blogPostCtx :: Context String
blogPostCtx =
    mconcat
        [ dateField "date" "%e %B, %Y"
        , teaserFieldSquareSeperator "preview" "content"
        , defaultContext
        ]

blogWriterOptions :: WriterOptions
blogWriterOptions =
    defaultHakyllWriterOptions
        { writerHTMLMathMethod = KaTeX ""
        , writerSectionDivs = True
        }

blogCompiler :: Compiler (Item String)
blogCompiler = pandocCompilerWith defaultHakyllReaderOptions blogWriterOptions

blogRoutes :: Rules ()
blogRoutes = do
    match "content/blog/*" $ do
        route $ stripContent $ setExtension "html"
        compile $ do
            blogCompiler
                >>= saveSnapshot "content"
                >>= loadAndApplyTemplate "templates/post.html" blogPostCtx
                >>= loadAndApplyTemplate "templates/default.html" blogPostCtx
                >>= teaserSquareSeparator
                >>= relativizeUrls

    create ["blog.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "content/blog/*"
            let blogPageContext =
                    listField "posts" blogPostCtx (return posts)
                        `mappend` constField "title" "Blog"
                        `mappend` defaultContext

            makeItem ""
                >>= loadAndApplyTemplate "templates/posts-page.html" blogPageContext
                >>= loadAndApplyTemplate "templates/default.html" blogPageContext
                >>= relativizeUrls
