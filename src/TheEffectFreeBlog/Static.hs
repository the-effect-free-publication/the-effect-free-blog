{-# LANGUAGE OverloadedStrings #-}

module TheEffectFreeBlog.Static (staticRoutes) where

import Hakyll

matchStatic :: Pattern -> Rules ()
matchStatic p = match p $ do
    route idRoute
    compile copyFileCompiler

staticRoutes :: Rules ()
staticRoutes = do
    matchStatic "static/images/**"

    matchStatic "static/files/**"

    matchStatic "static/scripts/**"

    matchStatic "static/styles/**"
