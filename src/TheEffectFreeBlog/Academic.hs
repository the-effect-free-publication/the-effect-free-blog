module TheEffectFreeBlog.Academic (academicPostCtx, academicsRoutes) where

import Hakyll
import TheEffectFreeBlog.Utils (compileAndLoadPost, stripContent)

academicPostCtx :: Context String
academicPostCtx =
    mconcat
        [ dateField "date" "%e %B, %Y"
        , teaserField "preview" "content"
        , defaultContext
        ]

academicsRoutes :: Rules ()
academicsRoutes = do
    match "content/resume.org" $ do
        route . stripContent $ setExtension "html"
        compile $ compileAndLoadPost defaultContext

    match "content/academic/*" $ do
        route $ stripContent $ setExtension "html"
        compile $ do
            pandocCompiler
                >>= loadAndApplyTemplate "templates/post.html" academicPostCtx
                >>= loadAndApplyTemplate "templates/default.html" academicPostCtx
                >>= relativizeUrls

    create ["academic.html"] $ do
        route idRoute
        compile $ do
            posts <- recentFirst =<< loadAll "content/academic/*"
            let academicPageContext =
                    listField "posts" academicPostCtx (return posts)
                        `mappend` constField "title" "Academic Papers and Notes"
                        `mappend` defaultContext

            getResourceBody
                >>= applyAsTemplate academicPageContext
                >>= loadAndApplyTemplate "templates/default.html" academicPageContext
                >>= relativizeUrls
