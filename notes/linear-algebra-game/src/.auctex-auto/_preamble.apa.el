(TeX-add-style-hook
 "_preamble.apa"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "a4paper" "margin=2cm") ("inputenc" "utf8") ("babel" "english") ("biblatex" "style=apa" "autocite=inline")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "geometry"
    "hyperref"
    "inputenc"
    "babel"
    "biblatex"
    "mathtools"
    "amsthm"
    "amssymb"
    "algorithm"
    "algpseudocode"
    "caption"
    "graphicx")
   (TeX-add-symbols
    '("Desc" 2))
   (LaTeX-add-bibliographies
    "trp-literature-review")
   (LaTeX-add-amsthm-newtheorems
    "definition"
    "example"
    "theorem"
    "lemma"
    "corollary"))
 :latex)

