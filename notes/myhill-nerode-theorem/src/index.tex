\section{Introduction}

Myhill-Nerode Theorem relates regular languages and equivalence classes. It is
an incredibly powerful way to prove that a language is or is not regular. There
is already an abundant amount of resources which prove the Myhill-Nerode Theorem
so here I focus on showing \textit{examples} of the Myhill-Nerode Theorem.

In order to describe Myhill-Nerode's theorem we first need to describe the
notion of a \textit{distinguishable} and \textit{indistinguishable} string.

\begin{definition}
  Given a alphabet $\Sigma$, let $L$ be a language over $\Sigma^{*}$. The words
  $x, y \in \Sigma^{*}$ are \textit{indistinguishable} by $L$ (denoted $x \equiv_{L} y$) if
  and only if for every word $z \in \Sigma^{*}$ either $xz \in L$ and $yz \in L$ or
  both $xz \notin L$ and $yz \notin L$. It should be trivial to show that $\equiv_{L}$ is an
  equivalence relation for the language $L$.
\end{definition}

It is also important to note the distinct difference between equality $x = y$,
an equivalence relation $x \equiv_{L} y$ and an \textit{equivalence class}. An
equivalence class is not \textit{strictly} equal but has the same properties of
things being equal. We can use \textit{equivalence relations} to partition sets,
and in this case we can use the \textit{indistinguishable} relation to partition
our language into subsets of the language where every word in the subset is
\textit{indistinguishable}. These are called \textit{equivalence classes} and
are vitally important to understanding Myhill-Nerode.

\begin{figure}[h]
  \centering
  \includegraphics[width=8cm]{equivalence-classes}
  \caption{Equivalence Class Partitions on the Language L}
\end{figure}

This leads to the following important notion.

\begin{definition}
  Given a alphabet $\Sigma$, let $L$ be a language over $\Sigma^{*}$. The words
  $x, y \in \Sigma^{*}$ are \textit{distinguishable} by $L$ if and only if there exists
  a word $z \in \Sigma^{*}$ such that $xz \in L$ and $yz \notin L$ or vice-versa.
\end{definition}

Note that the above is the direct opposite definition to a word being
\textit{indistinguishable}.

\begin{lemma}\label{lem:distinguishable}
  The number of equivalence classes is the same as the number of distinguishable
  words in a language.
\end{lemma}

\begin{proof}
  If two words are distinguishable with respect to a language $L$, then by
  definition they are not indistinguishable with each other. Hence they must be
  in different equivalence classes. It follows that every distinguishable word
  forms a separate equivalence \textit{class} (note this is not the same as an
  equivalence relation).
\end{proof}

\section{Myhill-Nerode Theorem}

We now have enough information to formally state the Myhill-Nerode theorem.

\begin{theorem}[Myhill-Nerode]
  A language $L$ is regular if and only if $\equiv_{L}$ has a finite number of
  equivalence classes.
\end{theorem}

This has the important counter definition which is very helpful in proving that
a language \textit{is not} regular.

\begin{corollary}
  If there exists an \textit{infinite} number of distinguishable words in a set
  with respect to $\equiv_{L}$ for a language $L$, then $L$ is not regular.
\end{corollary}

\begin{proof}
  By Lemma~\ref{lem:distinguishable}, if an infinite number of words
  \textit{distinguish} a language $L$, then the number of equivalence classes is
  infinite. By Myhill-Nerode Theorem, if a language has an infinite number of
  equivalence classes it cannot be regular.
\end{proof}

\begin{example-t}
  Prove the language $L_{a} = \{ 0^{n}1^{m}0^{n} \mid n,m \in \mathbb{N} \}$ is not regular.
\end{example-t}

\begin{proof}
  Let $S = \{ 0^{n}1 \mid n \in \mathbb{N} \}$ be an infinite set of words. For $i,j \in \mathbb{N}$
  where $i \neq j$, consider the string $0^{i}$, for the string $0^{i}1 \in S$ and
  $0^{j}1 \in S$, the string $0^{i}10^{i} \in L_{a}$ and $0^{j}10^{i} \notin L_{a}$ for
  every $i$ in $\mathbb{N}$. As such, there is an infinite number of distinguishable
  strings with respect to $L_{a}$, hence there is an infinite number of
  equivalence classes under $\equiv_{L_{a}}$ so $L_{a}$ cannot be regular.
\end{proof}

\begin{example-t}
  Prove the language
  $L_{b} = \{ 0^{i}1^{j}0^{k} \mid i,j,k \in \mathbb{N}, i = j \text{ or } j = k \}$ is not
  regular.
\end{example-t}

\begin{proof}
  Let $S = \{ a^{n}b^{n+1} \mid n \in \mathbb{N} \}$ be an infinite set of words. For $i,j \in \mathbb{N}$
  where $i \neq j$, consider the string $c^{i+1}$, for the string
  $a^{i}b^{i+1} \in S$ and $a^{j}b^{j+1} \in S$, the string
  $a^{i}b^{i+1}c^{i+1} \in L_{b}$ and $a^{j}b^{j+1}c^{i+1} \notin L_{b}$ for
  every $i$ in $\mathbb{N}$. As such, there is an infinite number of distinguishable
  strings with respect to $L_{b}$, hence there is an infinite number of
  equivalence classes under $\equiv_{L_{b}}$ so $L_{b}$ cannot be regular.
\end{proof}

\begin{example-t}
  Prove the language $L_{c} = \{ ww^{R} \mid w \in \{0,1\}^{*} \}$ is not regular.
\end{example-t}

\begin{proof}
  Let $S = \{ 0^{n}1 \mid n \in \mathbb{N} \}$ be an infinite set of words. For $i,j \in \mathbb{N}$
  where $i \neq j$, consider the string $10^{i}$, for the string $0^{i}1 \in S$ and
  $0^{j}1 \in S$, the string $0^{i}110^{i} \in L_{c}$ and $0^{j}110^{i} \notin L_{c}$ for
  every $i$ in $\mathbb{N}$. As such, there is an infinite number of distinguishable
  strings with respect to $L_{c}$, hence there is an infinite number of
  equivalence classes under $\equiv_{L_{c}}$ so $L_{c}$ cannot be regular.
\end{proof}

Notice the general strategy used here was to find an infinite set of strings
such that every string in the set was distinguishable. This implied that number
of equivalence classes is also infinite and by Myhill-Nerode the language could
not be regular.
