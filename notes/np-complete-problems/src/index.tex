\maketitle

\section{Intractability and NP-completeness}

In the study of the theory of computing there are some fundamental goals we set
out to achieve. One of the first goals was to ask are decision problems
\textit{decidable}. This let's us know if we can write an algorithm that solves
a problem. However, in practice knowing if \textit{there exists} an answer or
not is not useful unless we can find the answer \textit{before the heat death of
  the universe}.

This is the primary motivation for the notion of \textit{intractable} problems.
A problem is \textit{thought to be intractable} if it cannot be solved in a
\textit{reasonable amount of time}. What constitutes a reasonable amount of
time can be seen as the primary motivation for the complexity classes P and NP,
as well as the motivation for the complexity class NP-complete.

Before describing P vs NP, it is important to revisit the notion of determinism
vs non-determinism. We found out that for finite state automata, non-determinism
is equivalently as powerful as determinism. We also found out that deterministic
context free grammers are \textit{not} as powerful as non-deterministic context
free languages. So it was surprising to find out that all deterministic turing
machines are equivalently as powerful as non-deterministic turing machines in
terms of computability. However, this is not the complete picture. We also want
to know if problems that can be solved \textit{efficiently} on non-deterministic
turing machines can also be solved \textit{efficiently} on deterministic turing
machines.

\begin{definition}
  The complexity class P is the class of decision problems that are
  \textit{decidable} in \textit{polynomial time} ($O(n^{k})$), on a
  deterministic single-tape turing machine.
\end{definition}

\begin{definition}
  The complexity class NP is the class of decision problems that are
  \textit{decidable} in \textit{polynomial time} ($O(n^{k})$), on a
  non-deterministic single-tape turing machine.
\end{definition}

We can now see clearly that the question of P vs NP is really a question of
determinism vs non-determinism. There is also the equivalent definition of NP as
the class of problems that can be \textit{verified} in polynomial time on a
deterministic turing machine, which can be found in sipser.

Quite often we say an algorithm is \textit{efficient} if it is in P. One of the
biggest open questions in computer science that we are now (almost) equipped to
tackle is this problem of P vs NP.

\subsection{Aside: Proving Languages are in NP}

When I started in computer science one of my absolute favourite concepts was the
notion of a non-deterministic turing machine. The notion of non-determinism in
turing machines is also fundamental in proving that a language is in NP.

One of the most magical parts of a non-deterministic turing machine is it acts
like a normal turing machine augmented with a step usually referred to as
\textit{guess} or \textit{find} which can search for a solution to a problem
non-deterministically in polynomial time. How this \textit{actually} works can
be left as an excercise to the reader. Nevertheless the step can be described as
a peak into the future and finding an answer.

As a general strategy for proving a language is in NP we need to think about the
notion of \textit{verify}. Usually we follow the general framework of
\textit{non-deterministically guess a possible answer} and verify it in
polynomial time.

Recall that a \textit{GraphIsomorphism}
$\text{GraphIso} = \{ (G, H) \mid \exists \phi, \phi(G) = H \}$. This is the language that
given two graphs, accepts if there is a phi $\phi$ function that maps all the
vertices of $G$ to $H$ and preserves the edge relations. This has a
corresponding non-deterministic algorithm.

\begin{lemma}
  GraphIso is in NP.
\end{lemma}

\begin{proof}

  Consider the following algorithm.

\begin{algorithm}
  \textbf{Input: } $G, H$ are graphs

  \textbf{Output: } Accept or Reject

  \begin{enumerate}
    \item
          guess non-deterministically a isomorphic function $\phi$

    \item
          if $\phi(G) = H$, return Accept

    \item
          else return Reject
  \end{enumerate}
\end{algorithm}

Clearly the guess step is polynomial time bounded, the second step corresponds
to the definition of the language. If $\phi$ is an isomorphic function, and
$\phi(G) = H$, than by definition of GraphIso, the inputs are in the language,
while if there is no $\phi$, than step 1 would not be able to find the function and
the algorithm would reject.

\end{proof}

This general framework of first showing that you can guess an answer
non-deterministically, and than verify that the answer is in the language
defined is how we prove problems are in NP.

\subsection{Polynomial (Karp) Reductions and the Definition of NP-complete}

NP-complete is often cast as a problem of intractability but theoretically (I
think) it is really a question of determinism and non-determinism. A open
question is P = NP? How would we go about proving that P = NP. We first need to
describe reducibility amongst problems.

We have already seen that we can reduce a problem to another problem by finding
a computable function that \textit{changes the inputs} of a turing machine from
a turing machine that decides one language to a turing machine that decides
another. Formally,

\begin{definition}
  Let $A$, and $B$ be decision problems (or languages decided by turing
  machines). $A$ is \textit{mapping reducible} to $B$ denoted $A \leq_{m} B$ if and
  only if there exists a computable function $f$ such that for every

  \[
    w \in A \iff f(w) \in B
  \]

  and the function $f$ is called a \textit{reduction} from $A$ to $B$.
\end{definition}

This has a useful property that if there is a turing machine $M$ that can decide
$B$ there is a turing machine $N$ that can decide $A$ given by $N = f \circ M$. Or
simply we can find an algorithm that decides problem $A$ by using the algorithm
that decides problem $B$ and changing the inputs to the inputs of problem $A$.

We have to ask out of curiousity, is this really that helpful for proving P =
NP? Not really because this doesn't tell us anything about the complexity class
NP. We need to first define the notion of a polynomial time (karp) reduction.

\begin{definition}
  Let $A$ and $B$ be decision problems (or languages decided by a turing
  machine). $A$ is \textit{polynomial time reducible} to $B$ denoted $A \leq_{P} B$
  if and only if there exists a polynomial time computable function $f$ such
  that for every input string $w$ of $A$

  \[
    w \in A \iff f(w) \in B
  \]

  and the function $f$ is called a \textit{polynomial-time reduction} from $A$ to $B$.
\end{definition}

This seems similar to the definition of mapping reduction, however now we know
that if $f$ takes a polynomial amount of steps, than clearly if a problem $B$ is
in NP and $A$ is reducible to $B$ than $A$ must also be in NP by chaging the
inputs for $A$ to the inputs of $B$. It is also important to note that a
polynomial time reduction can be used to describe the degree of difficulty of a
problem. Clearly $B$ is harder than $A$ if $A$ is polynomial time reducible to
$B$ since if we can solve $B$ we can also solve $A$.

It is also important to note that when giving a polynomial time reduction it is
not sufficient to only give the algorithm. You \textit{must} also provide a
proof that the reduction \textit{is correct}, or that for every input of a
problem $B$ it changes the input to a problem $B$ and vice versa. You also must
provide a \textit{worst-case analysis} of the running time.

From here we can describe the notion of NP-hard and NP-complete. NP-hard can be
seen as atleast as hard as every problem in NP, and NP-complete is the hardest
problems in NP.

\begin{definition}
  A problem B is said to be \textit{NP-hard} if and only if for every problem A
  in NP, A is polynomial time reducible to B. A problem is \textit{NP-complete}
  if it is NP-hard and in NP.
\end{definition}

This definition has \textit{two} important consequences.

\begin{enumerate}
  \item
        If a problem in NP-complete can be solved in polynomial time, than P =
        NP.

  \item
        If a A problem is known to be NP-complete and another problem B is in NP
        and A is polynomial time reducible to B, than B must also be
        NP-complete.
\end{enumerate}

We now have a framework to both describe how to \textit{come up} with the
hardest problems in NP and to prove that P = NP.

\section{First NP-complete problems}

If we know some \textit{NP-complete} problems, than finding others is easy since
we can just reduce a known NP-complete problem to the new problem. So how would
we find our first NP-complete problem.

\begin{theorem}[Cook-Levin]
  SAT is NP-complete
\end{theorem}

Cook and Levin proved that SAT can be reduced in polynomial time to every
problem in NP. This is a foundational problem in theoretical computer science.
It allows us to find other NP-complete problems simply by reduction.

From SAT, the following problems are known to be NP-complete.

\begin{theorem}
  3-SAT is polynomial time reducible to SAT (as proved last week), so in this
  week's tutorial we show that the following problems are also NP-complete.

  \begin{enumerate}
    \item
          \textit{Independant Set}, decide if there exists a size $k$ subset of
          the vertices of a graph $G$ such that no two vertices are adjacent
          (Reduction given in lecture).
    \item
          \textit{Hamiltonian Path}, decide if there exists a path in a graph
          $G$ such that each vertex is visited once. (Reduction given in
          lecture).
    \item
          \textit{Subset Sum}, decide if there exists a subset of a set of
          integers such that they sum to some input value $N$. (Reduction in
          lecture).

    \item
          \textit{Vertex Cover}, decide if there exists a subset of the vertices
          of a graph G such that each vertex is connected to each edge in G.
          (Reduction from last weeks tutorial).
  \end{enumerate}
\end{theorem}

\section{Additional helpful notes}

\subsection{Space Complexity}

\subsection{Other Complexity Classes and the Polynomial Time Hierarchy}

\subsection{Different Types of Reductions}

\subsection{How to Handle Intractability}

\begin{enumerate}
  \item
        Talk about the complexity class co-NP.

  \item
        Talk about P-space.

  \item
        Talk about Karp and Turing reductions.

  \item
        Space Complexity.

  \item
        Dealing with intractability (use Luke's slides).
\end{enumerate}
