(TeX-add-style-hook
 "_preamble"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "a4paper" "margin=2cm")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "geometry"
    "hyperref"
    "mathtools"
    "amsthm"
    "amssymb"
    "algorithm")
   (LaTeX-add-environments
    "lemma"
    "theorem"
    "corollary"
    "definition"
    "example"))
 :latex)

